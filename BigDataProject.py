
# coding: utf-8

# # Start Spark
# this is to import all libraries and init a saprkSession to work with

# In[5]:


from datetime import datetime
import matplotlib.pyplot as plt


from pyspark.ml import Pipeline
from pyspark.ml.classification import DecisionTreeClassifier, RandomForestClassifier
from pyspark.ml.evaluation import MulticlassClassificationEvaluator, RegressionEvaluator
from pyspark.ml.feature import StringIndexer, OneHotEncoder, MinMaxScaler, VectorAssembler, VectorIndexer, StandardScaler
from pyspark.ml.stat import Correlation


from pyspark.sql import SparkSession, Row
from pyspark.sql.functions import udf, col, to_timestamp, hour, month, avg, sum, lit, to_date, hour, year
from pyspark.sql.types import DoubleType, FloatType, StringType, StructType, StructField, TimestampType, ArrayType, IntegerType


from pyspark.sql import functions as F
from pyspark.sql.types import *

import matplotlib.pyplot as plt
import six
import seaborn as sns
sns.set(color_codes=True)
import numpy as np
import pandas as pd



from pyspark.ml.feature import VectorAssembler, SQLTransformer
from pyspark.ml.evaluation import MulticlassClassificationEvaluator, BinaryClassificationEvaluator
from pyspark.ml.tuning import CrossValidator, ParamGridBuilder



# In[2]:


get_ipython().system(u' pip install Boruta')


# In[3]:


spark = SparkSession     .builder     .appName("Python Spark SQL basic example")     .config("spark.some.config.option", "some-value")     .getOrCreate()
  


# # Airport Delay Data Set
# 
# this data set has all data Flights in US from years 2009 to 2018 and was obtined from kaggle
# https://www.kaggle.com/yuanyuwendymu/airline-delay-and-cancellation-data-2009-2018#2012.csv
# 
# it is one file per year and the most relevant columns are
# 
# FL_DATE           (YYYY-MM-DD)    Date and time of the flight
# OP_CARRIER        (two letters)   Identifier of the operator(airline)
# OP_CARRIER_FL_NUM (4 numbers)     Nuber of the flight
# CRS_DEP_TIME      (HHmm.0)        Planed departure hour in local time
# DEP_DELAY         (mm.o)          Actual departure delay measured in minutes
# ORIGIN            (IATA code)     Origin airport identified by IATA code
# CRS_ARR_TIME      (HHmm.0)        Planed arrival hour in local time
# ARR_DELAY         (mm.o)          Actual arrival delay measured in minutes
# DEST              (IATA code)     Destination airport identified by IATA code
# 
# there are other columns that descrive other times like wheels on (the actual landing time) and taxi in (the actual arrival time) and the total fligh time and distance that are not interesting for our study
# 
# because the origin and destination are airports we need to do a lookup to get cities instead of codes,
# to do this we can work with this data set that index each airport in the world
# https://ourairports.com/data/ 
# 
# it is one file and the most relevant columns are
# 
# iata_code          IATA code (unique global identifier of each airport)
# municipality       city name
# type               type of airport (size)
# 
# we join the two datasets based on IATA code in destination and origin, finaly we save the merged data set in a single file in persistent storage

# In[ ]:


# definitions columns names to drop that are not usefull for the study
columns_to_drop_ap = [
    "id", "ident", "name", "latitude_deg", "longitude_deg", "elevation_ft", "continent", "iso_country",
    "iso_region", "scheduled_service", "gps_code", "local_code", "home_link",
    "wikipedia_link", "keywords"
]
columns_to_drop_delays = [
    "TAXI_OUT", "WHEELS_OFF", "WHEELS_ON", "TAXI_IN", "CANCELLED", "CANCELLATION_CODE", "DIVERTED",
    "CRS_ELAPSED_TIME", "ACTUAL_ELAPSED_TIME", "AIR_TIME", "DISTANCE", "CARRIER_DELAY",
    "WEATHER_DELAY",
    "NAS_DELAY",
    "SECURITY_DELAY", "LATE_AIRCRAFT_DELAY", "DEP_TIME", "ARR_TIME", "Unnamed: 27"
]


# In[ ]:


def remove_unused_columns(filename, columns_to_drop):
    dataset = spark.read.csv('gs://bigdatadelays/' + filename + '.csv', inferSchema=True, header=True)
    dataset = dataset.drop(*columns_to_drop)
    return dataset


def reformat_hours(date, float_hour):
    date = str(date)
    date = date.split(" ")
    date = date[0]
    float_hour = str(float_hour)
    float_hour = float_hour.split(".")
    float_hour = float_hour[0]
    try:
        hour = ":" + float_hour[-2] + float_hour[-1]
        if len(float_hour) == 4:
            hour = float_hour[0] + float_hour[1] + hour
        else:
            hour = "0" + float_hour[0] + hour
        return date + " " + hour
    except IndexError:
        return str(float_hour)


reformat_hours_string = udf(lambda d, h: reformat_hours(d, h), StringType())


def remove_useless_cities(airports):
    cities = {
        "Portland", "San Francisco", "Seattle", "Los Angeles", "San Diego", "Las Vegas", "Phoenix", "Albuquerque",
        "Denver", "San Antonio", "Dallas", "Houston", "Kansas City", "Minneapolis", "Saint Louis", "Chicago",
        "Nashville", "Indianapolis", "Atlanta", "Detroit", "Jacksonville", "Charlotte", "Miami", "Pittsburgh",
        "Toronto", "Philadelphia", "New York", "Montreal", "Boston"}
    return airports.where(col("municipality").isin(cities)).where(col("iata_code").isNotNull())


def merge_delays_and_airport(del_data, ap_data):
    res = merge_iata_city(True, del_data, ap_data)
    res = merge_iata_city(False, res, ap_data)
    return res


def merge_iata_city(is_origin, del_data, ap_data):
    dela = del_data.alias("dela")
    ap = ap_data.alias("ap")
    if is_origin:
        res = dela.join(ap, dela.ORIGIN == ap.iata_code).select("dela.*", "ap.municipality", "ap.type")
        res = res.drop(dela.ORIGIN)
        res = res.withColumnRenamed("municipality", "ORIGIN").withColumnRenamed("type", "ORIG_SIZE")
    else:
        res = dela.join(ap, dela.DEST == ap.iata_code).select("dela.*", "ap.municipality", "ap.type")
        res = res.drop(dela.DEST)
        res = res.withColumnRenamed("municipality", "DEST").withColumnRenamed("type", "DEST_SIZE")
    return res


def delays_treat_one_file(ap, i):
    delays = remove_unused_columns(str(i), columns_to_drop_delays)
    
    #formating departure and arrivel times to get datetime format
    delays = delays.withColumn('CRS_DEP_TIME', reformat_hours_string(delays.FL_DATE, delays.CRS_DEP_TIME))
    delays = delays.withColumn('CRS_ARR_TIME', reformat_hours_string(delays.FL_DATE, delays.CRS_ARR_TIME))  
    return merge_delays_and_airport(delays, ap)


def prepare_airport_datasets():
    #open airport data-set and remode unused columns
    simplified_ap = remove_unused_columns("airports", columns_to_drop_ap)
    simplified_ap = remove_useless_cities(simplified_ap)
    
    #open each delays data-set file and merge with airport
    res = delays_treat_one_file(simplified_ap, "2012")
    for i in range(2013, 2018):
        delays_airport = delays_treat_one_file(simplified_ap, i)
        res = res.union(delays_airport) 
    return res



delaystable = prepare_airport_datasets()

delaystable.write.csv("gs://bigdatadelays/bd_delays_merge", header=True)


# In[ ]:


delaystable.show()



# # Weather Data Set

# In[ ]:


cities_to_remove = [
    "Beersheba", "Tel Aviv District", "Eilat", "Haifa", "Nahariyya", "Jerusalem"
]

useful_cities = [
    "Portland", "San Francisco", "Seattle", "Los Angeles", "San Diego", "Las Vegas", "Phoenix", "Albuquerque",
    "Denver", "San Antonio", "Dallas", "Houston", "Kansas City", "Minneapolis", "Saint Louis", "Chicago",
    "Nashville", "Indianapolis", "Atlanta", "Detroit", "Jacksonville", "Charlotte", "Miami", "Pittsburgh",
    "Toronto", "Philadelphia", "New York", "Montreal", "Boston"
]

features = [
    ("temperature", DoubleType()),
    ("humidity", DoubleType()),
    ("weather_description", StringType()),
    ("wind_direction", DoubleType()),
    ("wind_speed", DoubleType())
]



# In[ ]:


cities_to_remove = [
    "Beersheba", "Tel Aviv District", "Eilat", "Haifa", "Nahariyya", "Jerusalem"
]

useful_cities = [
    "Portland", "San Francisco", "Seattle", "Los Angeles", "San Diego", "Las Vegas", "Phoenix", "Albuquerque",
    "Denver", "San Antonio", "Dallas", "Houston", "Kansas City", "Minneapolis", "Saint Louis", "Chicago",
    "Nashville", "Indianapolis", "Atlanta", "Detroit", "Jacksonville", "Charlotte", "Miami", "Pittsburgh",
    "Toronto", "Philadelphia", "New York", "Montreal", "Boston"
]

features = ["temperature","humidity","weather_description","pressure","wind_direction","wind_speed"]



def create_feature(dataset,nameFeature):
    columns = StructType([
        StructField('datetime', TimestampType(), False),
        StructField(nameFeature, DoubleType(), True),
        StructField('City', StringType(), False),
    ])
    datasetTemp0 = spark.createDataFrame(spark.sparkContext.emptyRDD(), columns)


    for i in range(len(useful_cities)):
        datasetTemp1 = dataset.select('datetime',useful_cities[i])        .withColumn('City', lit(useful_cities[i]))        .withColumnRenamed(useful_cities[i],nameFeature)

        datasetTemp0 = datasetTemp0.union(datasetTemp1)


    return datasetTemp0

def combine_features():
    columns = StructType([
        StructField('datetime', TimestampType(), False),
        StructField('City', StringType(), False),
    ])
    datasetFull0 = spark.createDataFrame(spark.sparkContext.emptyRDD(), columns)


    for i in range(len(features)):
        nameFeature=features[i]
        dataset = spark.read.csv('gs://bigdatadelays/' + nameFeature + '.csv', inferSchema=True, header=True)
        dataset = dataset.drop(*cities_to_remove)
        datasetFull1 = create_feature(dataset,nameFeature)
        datasetFull0 = datasetFull0.join(datasetFull1, on=["datetime","City"], how='outer')


    return datasetFull0

weatherMerged = combine_features()

weatherMerged.write.csv("gs://bigdatadelays/bd_cleaned_weather", header=True)


# In[ ]:


weatherMerged.show()


# # Merge

# In[ ]:


date_columns = ["CRS_DEP_TIME", "CRS_ARR_TIME"]


# In[ ]:



def open_dataset(filename):
    return spark.read.csv('gs://bigdatadelays/' + filename, inferSchema=True, header=True)


def add_dep_weather(delays, weather):
    d = delays.alias("d")
    w = weather.alias("w")
    res = d.join(w, [
        to_date(d.FL_DATE) == to_date(w.datetime),
        hour(to_timestamp(d.CRS_DEP_TIME)) == hour(w.datetime),
        d.ORIGIN == w.City
    ]).select("d.*", "w.temperature", "w.pressure", "w.humidity", "w.weather_description", "w.wind_direction", "w.wind_speed")
    res = res.withColumnRenamed("pressure", "ORI_PRESSURE")            .withColumnRenamed("humidity", "ORI_HUMIDITY")            .withColumnRenamed("temperature", "ORI_TEMPERATURE")            .withColumnRenamed("weather_description", "ORI_WEATHER_DESCR")            .withColumnRenamed("wind_direction", "ORI_WIND_DIR")            .withColumnRenamed("wind_speed", "ORI_WIND_SPEED")
    return res


def add_arr_weather(delays, weather):
    d = delays.alias("d")
    w = weather.alias("w")
    res = d.join(w, [
        to_date(d.FL_DATE) == to_date(w.datetime),
        hour(to_timestamp(d.CRS_ARR_TIME)) == hour(w.datetime),
        d.DEST == w.City
    ]).select("d.*", "w.temperature", "w.pressure",  "w.humidity", "w.weather_description", "w.wind_direction", "w.wind_speed")
    res = res.withColumnRenamed("pressure", "DEST_PRESSURE")        .withColumnRenamed("humidity", "DEST_HUMIDITY")        .withColumnRenamed("temperature", "DEST_TEMPERATURE")        .withColumnRenamed("weather_description", "DEST_WEATHER_DESCR")        .withColumnRenamed("wind_direction", "DEST_WIND_DIR")        .withColumnRenamed("wind_speed", "DEST_WIND_SPEED")
    return res


def extract_from_array(value):
    return float(value[0])

#defining a function
extract_from_array_udf = udf(lambda v: extract_from_array(v), DoubleType())



def rescaler(df, column):
    assembler = VectorAssembler(inputCols=[column], outputCol='column_ass')
    assembled = assembler.transform(df)
    scaler = MinMaxScaler(inputCol='column_ass', outputCol=column + "_S")
    scaler_model = scaler.fit(assembled)
    scaled_data = scaler_model.transform(assembled)
    df = scaled_data.drop("column_ass")
    ls_free = df.withColumn(column + "_S", extract_from_array_udf(df[column + "_S"]))
    return ls_free

def scale_features(df):
    df = rescaler(df, "ORI_TEMPERATURE")
    df = rescaler(df, "DEST_TEMPERATURE")
    df = rescaler(df, "ORI_HUMIDITY")
    df = rescaler(df, "DEST_HUMIDITY")
    df = rescaler(df, "ORI_PRESSURE")
    df = rescaler(df, "DEST_PRESSURE")
    df = rescaler(df, "ORI_WIND_SPEED")
    df = rescaler(df, "DEST_WIND_SPEED")
    df = rescaler(df, "ORI_WIND_DIR")
    df = rescaler(df, "DEST_WIND_DIR")
    return df

    
def formatting(df):
    pattern = '%Y-%m-%d %H:%M:%S'
    to_ts = udf(lambda x: datetime.strptime(x, pattern), TimestampType())
    to_float = udf(lambda x: float(x), DoubleType())
    for dc in date_columns:
        df = df.withColumn(dc, month(to_timestamp(df[dc])))
        df = df.withColumn(dc, to_float(df[dc]))
        df = rescaler(df, dc)
    return df


delays = open_dataset("bd_delays_merge")
weather = open_dataset("bd_cleaned_weather")
merged_data = add_dep_weather(delays, weather)
merged_data = add_arr_weather(merged_data, weather)

merged_data = merged_data.na.drop()

merged_data = scale_features(merged_data)
merged_data = formatting(merged_data)
merged_data.write.csv('gs://bigdatadelays/bd_global_prepared_dataset', header=True)
merged_data.show()


# # preparing dataset

# In[ ]:


vector_columns = ["ORI_WEATHER_DESCR_VEC", "DEST_WEATHER_DESCR_VEC", "ORIGIN_VEC", "DEST_VEC", "ORIG_SIZE_VEC",
                  "DEST_SIZE_VEC"]
attributes = [
        "CRS_DEP_TIME_S", "DEP_DELAY", "CRS_ARR_TIME_S", "ORIGIN_VEC", "ORIG_SIZE_VEC", "DEST_VEC", "DEST_SIZE_VEC",
        "ORI_WEATHER_DESCR_VEC", "DEST_WEATHER_DESCR_VEC", "ORI_HUMIDITY_S", "DEST_HUMIDITY_S", "ORI_TEMPERATURE_S", 
        "DEST_TEMPERATURE_S", "ORI_PRESSURE_S","DEST_PRESSURE_S", "ORI_WIND_SPEED_S", "DEST_WIND_SPEED_S", "ORI_WIND_DIR_S", "DEST_WIND_DIR_S"
    ]


# In[4]:



def open_dataset(filename):
    return spark.read.csv('gs://bigdatadelays/' + filename, inferSchema=True, header=True)


dense_format_udf = udf(lambda x: str(x), StringType())


def one_hot_encoding(df, column):
    string_indexer = StringIndexer(inputCol=column, outputCol=column + "_idx")
    model = string_indexer.fit(df)
    indexed = model.transform(df)
    encoder = OneHotEncoder(dropLast=True, inputCol=column + "_idx", outputCol=column + "_VEC")
    encoded = encoder.transform(indexed)
    simplified = encoded.drop(column + "_idx")
    # df = simplified.withColumn(column + "_VEC", dense_format_udf(col(column + "_VEC")))
    return simplified


def encode_strings(df):
    encoded = one_hot_encoding(df, "ORIGIN")
    encoded = one_hot_encoding(encoded, "ORIG_SIZE")
    encoded = one_hot_encoding(encoded, "DEST")
    encoded = one_hot_encoding(encoded, "DEST_SIZE")
    encoded = one_hot_encoding(encoded, "ORI_WEATHER_DESCR")
    df = one_hot_encoding(encoded, "DEST_WEATHER_DESCR")
    return df

    
def group_attributes(df):
    assembler = VectorAssembler(inputCols=attributes, outputCol='Attributes')
    output = assembler.transform(df)
    finalized_data = output.select("Attributes", "ARR_DELAY")
    return finalized_data


def formatting(df):
    assembler = VectorAssembler(inputCols=attributes, outputCol='Attributes')
    output = assembler.transform(df)
    finalized_data = output.select("Attributes", "ARR_DELAY")
    train_data, test_data = finalized_data.randomSplit([0.8, 0.2])
    return finalized_data, test_data, train_data


def evaluate_fitting(prediction):
    evaluator = RegressionEvaluator(labelCol="ARR_DELAY", predictionCol="prediction", metricName="rmse")

    # Root Mean Square Error
    rmse = evaluator.evaluate(prediction)
    print("RMSE: %.3f" % rmse)

    # Mean Square Error
    mse = evaluator.evaluate(prediction, {evaluator.metricName: "mse"})
    print("MSE: %.3f" % mse)

    # Mean Absolute Error
    mae = evaluator.evaluate(prediction, {evaluator.metricName: "mae"})
    print("MAE: %.3f" % mae)

    # r2 - coefficient of determination
    r2 = evaluator.evaluate(prediction, {evaluator.metricName: "r2"})
    print("r2: %.3f" % r2)


def plot_corr_matrix(finalized_data, fig_no):
    r1 = Correlation.corr(finalized_data, "Attributes").collect()[0][0]
    corrmatrix = r1.toArray().tolist()
    fig = plt.figure(fig_no)
    ax = fig.add_subplot(111)
    ax.set_title("Correlation Matrix for Attributes")
    ax.set_xticklabels([''])
    ax.set_yticklabels([''])
    cax = ax.matshow(corrmatrix, vmax=1, vmin=-1)
    fig.colorbar(cax)
    plt.show()


evaluate_gap_udf = udf(lambda x, y: abs(x-y), DoubleType())


def decision_tree_classifier(train_data, test_data):
    label_indexer = StringIndexer(inputCol="ARR_DELAY", outputCol="indexedLabel",
                                  handleInvalid="skip").fit(
        train_data)
    feature_indexer =         VectorIndexer(inputCol="Attributes", outputCol="indexedFeatures", maxCategories=48, handleInvalid="skip").fit(
            train_data)
    classifier = DecisionTreeClassifier(featuresCol='indexedFeatures', labelCol='indexedLabel')
    pipeline = Pipeline(stages=[label_indexer, feature_indexer, classifier])

    # Train model.  This also runs the indexers.
    model = pipeline.fit(train_data)

    # Make predictions.
    predictions = model.transform(test_data)

    evaluate_fitting(predictions)
    eval_pred = predictions.withColumn("GAP", evaluate_gap_udf(predictions["ARR_DELAY"], predictions["prediction"]))
    average = eval_pred.select(avg(eval_pred.GAP)).collect()[0][0]
    print("average " + str(average))
    
    
    

#print("start")
#df = open_dataset("bd_global_prepared_dataset")
#df = df.na.drop()
#dt = df.alias("dt")
#train_data = df.select("*").where(year(to_timestamp(dt.FL_DATE)) == 2013)
#test_data = df.select("*").where(year(to_timestamp(dt.FL_DATE)) == 2014)
#print(train_data.count())
#print(test_data.count())


#train_data = encode_strings(train_data)
#test_data = encode_strings(test_data)


#train_data = group_attributes(train_data)
#test_data = group_attributes(test_data)

#     decision_tree_classifier(train_data, test_data)



# # Train dataset

# In[ ]:


vector_columns = ["ORI_WEATHER_DESCR_VEC", "DEST_WEATHER_DESCR_VEC", "ORIGIN_VEC", "DEST_VEC", "ORIG_SIZE_VEC",
                  "DEST_SIZE_VEC"]
attributes = [
        "CRS_DEP_TIME_S", "DEP_DELAY", "CRS_ARR_TIME_S", "ORIGIN_VEC", "ORIG_SIZE_VEC", "DEST_VEC", "DEST_SIZE_VEC",
        "ORI_WEATHER_DESCR_VEC", "DEST_WEATHER_DESCR_VEC", "ORI_HUMIDITY_S", "DEST_HUMIDITY_S", "ORI_TEMPERATURE_S", 
        "DEST_TEMPERATURE_S", "ORI_PRESSURE_S","DEST_PRESSURE_S", "ORI_WIND_SPEED_S", "DEST_WIND_SPEED_S", "ORI_WIND_DIR_S", "DEST_WIND_DIR_S"
    ]


# In[ ]:




def open_dataset(filename):
    return spark.read.csv('gs://bigdatadelays/' + filename, inferSchema=True, header=True)


dense_format_udf = udf(lambda x: str(x), StringType())


def one_hot_encoding(df, column):
    string_indexer = StringIndexer(inputCol=column, outputCol=column + "_idx")
    model = string_indexer.fit(df)
    indexed = model.transform(df)
    encoder = OneHotEncoder(dropLast=True, inputCol=column + "_idx", outputCol=column + "_VEC")
    encoded = encoder.transform(indexed)
    simplified = encoded.drop(column + "_idx")
    # df = simplified.withColumn(column + "_VEC", dense_format_udf(col(column + "_VEC")))
    return simplified


def encode_strings(df):
    print('encode string')
    encoded = one_hot_encoding(df, "ORIGIN")
    encoded = one_hot_encoding(encoded, "ORIG_SIZE")
    encoded = one_hot_encoding(encoded, "DEST")
    encoded = one_hot_encoding(encoded, "DEST_SIZE")
    encoded = one_hot_encoding(encoded, "ORI_WEATHER_DESCR")
    df = one_hot_encoding(encoded, "DEST_WEATHER_DESCR")
    return df


def prepare_dataset():
    print("start")
    # df = open_dataset("sample")
    df = open_dataset("bd_global_prepared_dataset")
    data = df.na.drop()
    data = encode_strings(data)
    finalized_data, test_data, train_data = formatting(data)
    decision_tree_regression(train_data, test_data)
    gradient_boosted_regressor(train_data, test_data)
    gradient_boosted_regressor_cross_validation(train_data, test_data)
    random_forest_regressor(train_data, test_data)


    
def describe_data(finalized_data):
    # parse = udf(lambda s: Vectors.parse(s).asML(), VectorUDT())
    finalized_data.printSchema()
    features_name = finalized_data.columns[1:]
    plt.figure(figsize=(24,23))
    sns.heatmap(finalized_data.select([c for c in features_name]).toPandas().corr(),                linewidths=0.1,vmax=1.0, square=True, linecolor='white', annot=True)
    plt.show()
    plt.gcf().clear()
    
    
def group_attributes(df):
    print('group attributes')
    assembler = VectorAssembler(inputCols=attributes, outputCol='Attributes')
    output = assembler.transform(df)
    finalized_data = output.select("Attributes", "ARR_DELAY", "FL_DATE")
    return finalized_data


def formatting(df):
    print('start formatting')
    assembler = VectorAssembler(inputCols=attributes, outputCol='Attributes')
    output = assembler.transform(df)
    finalized_data = output.select("Attributes", "ARR_DELAY")
    train_data, test_data = finalized_data.randomSplit([0.8, 0.2])
    return finalized_data, test_data, train_data


def evaluate_fitting(prediction):
    evaluator = RegressionEvaluator(labelCol="ARR_DELAY", predictionCol="prediction", metricName="rmse")

    # Root Mean Square Error
    rmse = evaluator.evaluate(prediction)
    print("RMSE: %.3f" % rmse)

    # Mean Square Error
    mse = evaluator.evaluate(prediction, {evaluator.metricName: "mse"})
    print("MSE: %.3f" % mse)

    # Mean Absolute Error
    mae = evaluator.evaluate(prediction, {evaluator.metricName: "mae"})
    print("MAE: %.3f" % mae)

    # r2 - coefficient of determination
    r2 = evaluator.evaluate(prediction, {evaluator.metricName: "r2"})
    print("r2: %.3f" % r2)


def plot_corr_matrix(finalized_data, fig_no):
    r1 = Correlation.corr(finalized_data, "Attributes").collect()[0][0]
    corrmatrix = r1.toArray().tolist()
    fig = plt.figure(fig_no)
    ax = fig.add_subplot(111)
    ax.set_title("Correlation Matrix for Attributes")
    ax.set_xticklabels([''])
    ax.set_yticklabels([''])
    cax = ax.matshow(corrmatrix, vmax=1, vmin=-1)
    fig.colorbar(cax)
    plt.show()


evaluate_gap_udf = udf(lambda x, y: abs(x-y), DoubleType())


def feature_selection(train_data):
    selector = ChiSqSelector(featuresCol="Attributes",
                             outputCol="selectedFeatures", labelCol="ARR_DELAY").setSelectorType("fpr")
    result = selector.fit(train_data).transform(train_data)
    result.show()
    return result


def decision_tree_classifier(train_data, test_data):
    print("decision tree classifier")
    label_indexer = StringIndexer(inputCol="ARR_DELAY", outputCol="indexedLabel", handleInvalid="skip").fit(
        train_data)
    feature_indexer =         VectorIndexer(inputCol="Attributes", outputCol="indexedFeatures", maxCategories=4, handleInvalid='skip').fit(
            train_data)
    classifier = DecisionTreeClassifier(featuresCol='indexedFeatures', labelCol='indexedLabel')
    pipeline = Pipeline(stages=[label_indexer, feature_indexer, classifier])

    # Train model.  This also runs the indexers
    print("start training")
    model = pipeline.fit(train_data)

    # Make predictions.
    print("start predict")
    predictions = model.transform(test_data)
    
    print("start evaluate fitting")
    evaluate_fitting(predictions)
    print("start evaluate gap")
    eval_pred = predictions.withColumn("GAP", evaluate_gap_udf(predictions["ARR_DELAY"], predictions["prediction"]))
    average = eval_pred.select(avg(eval_pred.GAP)).collect()[0][0]
    print("average gap: " + str(average))
    
    
def decision_tree_regression(train_data, test_data):
    print("decision tree regressor")
    feature_indexer =         VectorIndexer(inputCol="Attributes", outputCol="indexedFeatures", maxCategories=4,
                      handleInvalid="skip").fit(
            train_data)
    regressor = DecisionTreeRegressor(featuresCol='indexedFeatures', labelCol='ARR_DELAY')
    pipeline = Pipeline(stages=[feature_indexer, regressor])

     # Train model.  This also runs the indexers
    print("start training")
    model = pipeline.fit(train_data)

    # Make predictions.
    print("start predict")
    predictions = model.transform(test_data)
    
    print("start evaluate fitting")
    evaluate_fitting(predictions)
    print("start evaluate gap")
    eval_pred = predictions.withColumn("GAP", evaluate_gap_udf(predictions["ARR_DELAY"], predictions["prediction"]))
    average = eval_pred.select(avg(eval_pred.GAP)).collect()[0][0]
    print("average gap: " + str(average))
    
    
def gradient_boosted_regressor(train_data, test_data):
    print("Gradient boosted regressor")
    feature_indexer =         VectorIndexer(inputCol="Attributes", outputCol="indexedFeatures", maxCategories=4,
                      handleInvalid="skip").fit(
            train_data)
    regressor = GBTRegressor(featuresCol='indexedFeatures', labelCol='ARR_DELAY', maxIter = 20)
    pipeline = Pipeline(stages=[feature_indexer, regressor])

     # Train model.  This also runs the indexers
    print("start training")
    model = pipeline.fit(train_data)

    # Make predictions.
    print("start predict")
    predictions = model.transform(test_data)
    
    print("start evaluate fitting")
    evaluate_fitting(predictions)
    print("start evaluate gap")
    eval_pred = predictions.withColumn("GAP", evaluate_gap_udf(predictions["ARR_DELAY"], predictions["prediction"]))
    average = eval_pred.select(avg(eval_pred.GAP)).collect()[0][0]
    print("average gap: " + str(average))
    
    
def gradient_boosted_regressor_cross_validation(train_data, test_data):
    print("Gradient boosted regressor with cross validation")
    feature_indexer =         VectorIndexer(inputCol="Attributes", outputCol="indexedFeatures", maxCategories=4,
                      handleInvalid="skip").fit(
            train_data)
    regressor = GBTRegressor(featuresCol='indexedFeatures', labelCol='ARR_DELAY')
    pipeline = Pipeline(stages=[feature_indexer, regressor])
    
    paramGrid = ParamGridBuilder()     .addGrid(regressor.maxIter, [20])     .build()
    
    evaluator = RegressionEvaluator(labelCol="ARR_DELAY", predictionCol="prediction", metricName="mse")
    
    print("start cross validation")
    crossval = CrossValidator(estimator=pipeline,
                          estimatorParamMaps=paramGrid,
                          evaluator=evaluator,
                          numFolds=5)  # use 3+ folds in practice

     # Train model.  This also runs the indexers
    print("start training")
    model = crossval.fit(train_data)

    # Make predictions.
    print("start predict")
    predictions = model.transform(test_data)
    
    print("start evaluate fitting")
    evaluate_fitting(predictions)
    print("start evaluate gap")
    eval_pred = predictions.withColumn("GAP", evaluate_gap_udf(predictions["ARR_DELAY"], predictions["prediction"]))
    average = eval_pred.select(avg(eval_pred.GAP)).collect()[0][0]
    print("average gap: " + str(average))
    
    
    
def random_forest_regressor(train_data, test_data):
    print("random forest regressor")
    feature_indexer =         VectorIndexer(inputCol="Attributes", outputCol="indexedFeatures", maxCategories=4,
                      handleInvalid="skip").fit(
            train_data)
    regressor = RandomForestRegressor(featuresCol='indexedFeatures', labelCol='ARR_DELAY')
    pipeline = Pipeline(stages=[feature_indexer, regressor])

     # Train model.  This also runs the indexers
    print("start training")
    model = pipeline.fit(train_data)

    # Make predictions.
    print("start predict")
    predictions = model.transform(test_data)
    
    print("start evaluate fitting")
    evaluate_fitting(predictions)
    print("start evaluate gap")
    eval_pred = predictions.withColumn("GAP", evaluate_gap_udf(predictions["ARR_DELAY"], predictions["prediction"]))
    average = eval_pred.select(avg(eval_pred.GAP)).collect()[0][0]
    print("average gap: " + str(average))
    
prepare_dataset()


# In[ ]:


train_data.printSchema()

features = [f for f in train_data.columns if f not in ['ARR_DELAY']]
len(features)


plt.figure(figsize=(24,23))
sns.heatmap(train_data.select(features).toPandas().corr(),linewidths=0.1,vmax=1.0, square=True, linecolor='white', annot=True)
plt.show()
plt.gcf().clear()
    

    
    



# Boruta

# In[ ]:


import numpy as np 
import pandas as pd 
from sklearn.ensemble import RandomForestClassifier
import matplotlib.pyplot as plt


df = open_dataset("bd_global_prepared_dataset")
df = df.na.drop()
dt = df.alias("dt")


# In[ ]:


train_data = df.select("*").where(year(to_timestamp(dt.FL_DATE)) == 2014)
train_Pandas = train_data.toPandas()
X_train, y_train = train_Pandas.drop(['ARR_DELAY','FL_DATE','ORIGIN','DEST'], axis=1), df['ARR_DELAY']


# In[ ]:


test_data = df.select("*").where(year(to_timestamp(dt.FL_DATE)) == 2015)
test_Pandas = test_data.toPandas()
X_test, y_test = test_Pandas.drop(['ARR_DELAY','FL_DATE','ORIGIN','DEST'], axis=1), df['ARR_DELAY']


# In[ ]:


class MultiColumnLabelEncoder:
    
    def __init__(self, columns = None):
        self.columns = columns # list of column to encode
    def fit(self, X, y=None):
        return self
    def transform(self, X):
        '''
        Transforms columns of X specified in self.columns using
        LabelEncoder(). If no columns specified, transforms all
        columns in X.
        '''
        
        output = X.copy()
        
        if self.columns is not None:
            for col in self.columns:
                output[col] = LabelEncoder().fit_transform(output[col])
        else:
            for colname, col in output.iteritems():
                output[colname] = LabelEncoder().fit_transform(col)
        
        return output
    def fit_transform(self, X, y=None):
        return self.fit(X, y).transform(X)


# In[ ]:


from sklearn.preprocessing import LabelEncoder

le = MultiColumnLabelEncoder()
X_train_le = le.fit_transform(X_train)
X_train_le.head()


# In[ ]:


datasetTemp0 = spark.createDataFrame(X_train_le)


# In[ ]:


datasetTemp0.write.csv('gs://bigdatadelays/X_train_le', header=True)


# In[6]:


import numpy as np 
import pandas as pd 
from sklearn.ensemble import RandomForestClassifier
import matplotlib.pyplot as plt


X_train_le = open_dataset("X_train_le")


# In[ ]:


X_train_le_pd=X_train_le.toPandas()


# In[ ]:


from sklearn.preprocessing import OneHotEncoder
ohe = OneHotEncoder(sparse=False)
X_train_ohe = ohe.fit_transform(X_train_le.toPandas())
X_train_ohe


# In[ ]:


import seaborn as sns
from sklearn.metrics import accuracy_score, confusion_matrix
from sklearn.linear_model import LogisticRegression
#==============================================================================
# All features
#==============================================================================
est = LogisticRegression()
est.fit(X_train, y_train)

acc = accuracy_score(y_test, est.predict(X_test))
print('Test Accuracy {}'.format(acc))

cm = confusion_matrix(y_test, est.predict(X_test))
sns.heatmap(cm, fmt='d', cmap='GnBu', cbar=False, annot=True)


# In[ ]:


df_HotEncoded = pd.get_dummies(imputeDF_Pandas, drop_first=True, dummy_na=True)
df_HotEncoded.shape

features = [f for f in df_HotEncoded.columns if f not in ['ARR_DELAY']]
len(features)


# In[ ]:





import seaborn as sns
from sklearn.metrics import accuracy_score, confusion_matrix
from sklearn.linear_model import LogisticRegression
#==============================================================================
# All features
#==============================================================================
est = LogisticRegression()
est.fit(X_train, y_train)

acc = accuracy_score(y_test, est.predict(X_test))
print('Test Accuracy {}'.format(acc))

cm = confusion_matrix(y_test, est.predict(X_test))
sns.heatmap(cm, fmt='d', cmap='GnBu', cbar=False, annot=True)


# In[ ]:


X = df_HotEncoded[features].values
Y = df_HotEncoded['ARR_DELAY'].values.ravel()


# In[ ]:


rf = RandomForestClassifier(n_jobs=-1, class_weight='balanced', max_depth=5)


# In[ ]:


boruta_feature_selector = BorutaPy(rf, n_estimators='auto', verbose=2, random_state=4242, max_iter = 50, perc = 90)
boruta_feature_selector.fit(X, Y)


# In[ ]:


# example of normalizing input and output variables for regression.
from numpy import mean
from numpy import absolute
from numpy import loadtxt
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.pipeline import Pipeline
from sklearn.linear_model import HuberRegressor
from sklearn.preprocessing import MinMaxScaler
from sklearn.compose import TransformedTargetRegressor
# load data
dataset = X_train_le_pd.to_numpy()
# split into inputs and outputs
X, y = dataset[:, :-1], dataset[:, -1]
# prepare the model with input scaling
pipeline = Pipeline(steps=[('normalize', MinMaxScaler()), ('model', HuberRegressor())])
# prepare the model with target scaling
model = TransformedTargetRegressor(regressor=pipeline, transformer=MinMaxScaler())
# evaluate model
cv = KFold(n_splits=10, shuffle=True, random_state=1)
scores = cross_val_score(model, X, y, scoring='neg_mean_absolute_error', cv=cv, n_jobs=-1)
# convert scores to positive
scores = absolute(scores)
# summarize the result
s_mean = mean(scores)
print('Mean MAE: %.3f' % (s_mean))


# In[ ]:


get_ipython().system(u'conda update scikit-learn')

